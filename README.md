# A Corpus Describing Orchestral Texture in First Movements of Classical and Early-Romantic Symphonies

Orchestration is the art of writing music for a possibly large ensemble of instruments, by blending or opposing their sounds and grouping them into an orchestral texture. We aim here at providing a deeper understanding of orchestration in classical and early-romantic symphonies by analyzing, at the bar level, how the instruments of the orchestra organize into melodic, rhythmic, harmonic, and mixed layers. We formalize the description of such layers and release an open corpus with more than 7900 annotations in 24 first movements of Haydn, Mozart, and Beethoven symphonies. Initial analyses of this corpus confirm specific roles of the instruments and their families (woodwinds, brass, and strings), some evolution between composers, as well as the contribution of orchestral texture to form. The model and the corpus offer perspectives for empirical and computational studies on orchestral music.

Current release:

- annotations, as `.orch` files in `annotations/<composer>`
- annotations, as `.dez` files in `annotations/<composer>`
- Digital scores (`.krn`, `.musicxml` files) in `scores/<composer>` :
  - Missing files for Mozart Symphonies 32 to 36 (K.318, K.319, K.338, K.385 and K.425)
- Source code used to parse the annotations and to produce some figures of the paper in `src/`

## Reference

Dinh-Viet-Toan Le, Mathieu Giraud, Florence Levé, Francesco Maccarini.
*A Corpus Describing Orchestral Texture in First Movements of Classical and Early-Romantic Symphonies,*
Digital Libraries for Musicology (DLfM 2022),
<https://dx.doi.org/10.1145/3543882.3543884>

# Syntax of the `.orch` files

## Instruments

| Instrument name | Syntax   |
|-----------------|----------|
| Flute           | ``Fl``   |
| Oboe            | ``Ob``   |
| Clarinet        | ``Cl``   |
| Bassoon         | ``Fg``   |
| Horn            | ``Hrn``  |
| Trumpet         | ``Trp``  |
| Timpani         | ``Timp`` |
| Violin 1        | ``Vln1`` |
| Violin 2        | ``Vln2`` |
| Viola           | ``Vla``  |
| Cello           | ``Vc``   |
| Double bass     | ``Cb``   |

Divisi instruments are annotated with an additional number, such as ``Ob2``.

An annotation file begins with the definition of the list of instruments used in the
following lines, using this format:

```
InstList: Wood:Fl.Ob.Cl.Fg|Brass:Hrn.Trp|Perc:Timp|Strings:Vln1.Vln2.Vla.Vc.Cb
```


## Relations
| Relation      | Syntax |
|---------------|--------|
| Homorhythm    | ``-h`` |
| Parallel move | ``-p`` |
| Octave or Unisson doublings | ``-u`` |


## Roles

| Role and subrole                  | Syntax          |
|-----------------------------------|-----------------|
| __Melody__                        | ``mel``         |
| > Imitation                       | ``mel::imitation``         |
| __Rhythmic accompaniment__        | ``rhythm``      |
| > Repeated notes                  | ``rhythm::repeat_note`` |
| > Oscillation                     | ``rhythm::osc``         |
| > "Batterie"                      | ``rhythm::batt``        |
| > Arpeggio                        | ``rhythm::arp``         |
| > Scale                           | ``rhythm::scale``       |
| __Harmonic accompaniment__        | ``harm``        |
| __Mixed__                         |                 |
| > Decorative melody               | ``mel+rhythm::decmel``      |
| > Sparse elements / Sparse chords | ``harm+rhythm::sparse``      |
|                                   |                 |

The sub-roles are optional.
When a sub-role is used, the main role can be omitted for brevity,
such as ``decmel`` instead of ``mel+rhythm::decmel``.

Optional rhythm precision can be added to a role, such as:
- ``rhythm16::repeat_note`` : Rhythmic accompaniment composed of semiquavers
- ...

## Layers

A layer is composed of an identifier that gathers instruments, their relation and an optional role (if the role is omitted, its role is considered as ``None``).
_Layers_ are annotated as : ``identifier:role-relation`` or ``identifier:-relation``

A `~` can be added ``~id:role-relation``, meaning that the layer continues from a previous segment, as described below.

## Identifiers

An identifier is a letter used to group instruments belonging to the same layer. We adopt the following (non-compulsory) convention for chosing the letter.

| Layer characteristics (role or other) | Letters               |
|---------------------------------------|-----------------------|
| Melody                                | ``a, b, c, d, ...``   |
| Rhythmic accompaniment                | ``r, s, u, v, ...``   |
| Harmonic accompaniment                | ``h, i, l, m, ...``   |
| Tutti (all or almost all instuments)  | ``t``                 |

## Segment

A segment is composed of mulitple layers.
_Segments_ are annotated as : ``[bar_range] <identifiers> layer1 layer2 ... {specification}``

- ``bar_range`` is the range of the segment
- The order of ``<identifiers>`` is defined by the ``InstList`` at the beginning of the file.
- The optional ``{specification}`` can describe further properties or effects such as CR   (Call-and-response). These effects are not the focus of this study.


> __Example__ : 
> ```
> [5]   <h000|h0|0|rrrr0>   h:harm-p r:repeat_note-h
> ```
> Using the same heading ``InstList`` defined above, at bar 5, two layers are heard :
> - A layer (identified by `h:`) composed of flutes and horns playing sustained harmonic in parallel motion.
> - A layer (identified by `r:`) composed of violins 1 & 2, violas and cellos playing repeated notes in homorhythm.

Layers that continues through different segments can be annotated as ``~layer``.


> __Example__ 
> ```
> [5]     <h000|h0|0|rrrr0>   h:harm-p r:repeat_note-h
> [6-7]   <h000|00|0|aa000>   ~h:harm-p a:mel-u
> ```
> - The harmonic layer firstly played by flutes and horns in bar 5 continues on bars 6 to 7, keeping only the flutes.


Divisi (with different layer/roles) can be entered with parentheses, such as `<(ha)000|h0|0|rrrr0>`,
with divisi on the flutes.

## Bar range

A bar range can specified by any combination of single measures or range,
such as ``[1-6]``, ``[4]``, ``[1-6,8]``, or ``[1-6,15-17,19]``.


## Meta-layers

The optional ``{specification}`` can describe further properties or effects such as CR (Call-and-response). The typical structure is the following.

``{XX frequency (identifiers1)[bar-list](identifiers2)[bar-list]...}``

- ``XX`` indicates the kind of effect, for example ``CR`` (Call-and-Response) or ``TE`` (Timbral Echoes),
- ``fequency`` is a number that has a different meaning according to the effect,
- ``identifiers`` contains a list of identifiers (parenthesis are omitted if only one identifier is present) and
- ``bar-list`` contains the bars where the previously stated identifiers are found. 

> __Example__
> ```
> {CR 1 a[595,597]a{596,598}}
>```
> This means that there is a call and response scheme in bars 595-598 between the instruments identified with ``a`` in bars 595 and 597, and the instruments identified with ``a`` in bars 596 and 598. The call and the response alternate with a frequency of 1 bar.

## Score

The full score is composed of segments.

- Comments are annotated as ``# Comment``
- Formal sections are annotated beginning with ``#!label Comment`` 
  - In this case, segments between a line ``#!label1`` and ``#!label2`` are labeled as ``label1``
- Repetitions are annotated beginning with ``# REP |:`` and ending with ``# REP :|``. In case the repetion requires different endings the beginning of the first case is annotated as ``# REP 1-``, and the beginning of the second case as ``# REP 2-``.
